package Module;

import java.util.Comparator;

/**
 * 
 * @author 18anaa01
 *
 */

public class WordFrequency implements Comparator<Object> {

	private String word = "";
	private int frequency;
	
	
	public WordFrequency(String word) {
		this.word = word;
	}
	
	public void count() {
		this.frequency++;
	}
	
	public String getWord() {
		return word; 
	}
	
	public int getFrequency() {
		return frequency;
	}

/*	@Override
	public int compare(Object o1, Object o2) {
		WordFrequency w1 = (WordFrequency)o1;
		WordFrequency w2 = (WordFrequency)o2;
		
		if(w1.frequency == w2.frequency) {
			return 0;
		}
		else if(w1.frequency < w2.frequency) {
			return 1;
		}
		else {
			return -1; 
		}
	}*/
	
	@Override
	public String toString() {
		return this.word;
	}

	@Override
	public int compare(Object o1, Object o2) {
//		// TODO Auto-generated method stub
//		return (w1, w2) -> w1.getWord().compareTo( w2.getWord() ) ;
		return 0;
	}
	
}
