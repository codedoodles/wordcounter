package Module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;

public class Main {
	static final String CHAR_FILTER = "[.,&!\"'\\|_\\-()\\[\\]\\/:;?*#$“”‘—\\s+]";
	static final String NUM_FILTER = "[1234567890%#]";
	static int totalWords = 0;
	
	static public double countPercentage(int wordCount) {
		double total = (((double) wordCount / totalWords ) * 100 );
		return total;
	}
	
	public static void main(String[] args) {
		

		//Open File
		File file = new File("C:\\Users\\anous\\Documents\\Programming\\alice.txt");
		StringBuilder textString = new StringBuilder();
		
		try {
			Scanner scanner = new Scanner(file);
			
			while (scanner.hasNextLine()) {
				textString.append(" " + scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println("No file with that name found.");
			e.printStackTrace();
		}
		
		//Convert between Stringbuilder > String > Remove signs > String array
		String longString = textString.toString();
		
		//RegEx
		longString = longString.replaceAll(CHAR_FILTER, " ");
		longString = longString.replaceAll(NUM_FILTER, " ");
		
		String[] stringArray = longString.split(" ");
		
		for(String word : stringArray) {
			word.trim();
		}
		
		HashMap<String, WordFrequency> bookWordsInventory = new HashMap<String, WordFrequency>();
		
		// Compare the keyword with the textfile, count frequency, add the words to a HashMap
		for(int i = 0; i < stringArray.length; i++) {
			String keyword = stringArray[i].toLowerCase();
			
			if(bookWordsInventory.containsKey(keyword) ) {
				bookWordsInventory.get(keyword).count(); 
				totalWords++;
				}
			
			else {
				WordFrequency newWord = new WordFrequency(keyword);
				bookWordsInventory.put(keyword, newWord);
				bookWordsInventory.get(keyword).count();
				totalWords++;
			}
		}
		//Clear the extra nothings that have been left over somehow...
		//Not sure why, not sure how to fix otherwise. Suggestions welcome!
		totalWords = totalWords - (bookWordsInventory.get("").getFrequency() );
		bookWordsInventory.remove("");
		
		List<WordFrequency> wordList = new ArrayList<>(bookWordsInventory.values()); 
		
		//Formatting decimal printout
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		
		//---  Start of print out --- //
		System.out.println("Output of " + file.getName());
		
		//Alphabetical sorting
		wordList.sort( ( (w1, w2) -> w1.getWord().compareTo( w2.getWord() ) ) );
		wordList.forEach( (WordFrequency) ->
					System.out.println ( WordFrequency.getFrequency() 
					+ "\t -> \t" 
					+ ( df.format( (countPercentage(WordFrequency.getFrequency()) )) + " %")
					+ "\t -> \t "  
					+ WordFrequency.getWord() 
					) 
				);
	
		//Sorted by frequency
		wordList.sort( ( (w1,w2) -> w1.getFrequency() - ( w2.getFrequency() ) ) );
		wordList.forEach((WordFrequency) -> 
					System.out.println(WordFrequency.getFrequency() 
					+ "\t -> \t" 
					+ ( df.format( (countPercentage(WordFrequency.getFrequency()) )) + " %")
					+ "\t -> \t " 
					+  WordFrequency.getWord()
					) 
				);
							
		System.out.println("Total Uniqe Words:" + wordList.size() + "  Total words: " + totalWords);
		System.out.println("Unique Words of All Words: " + df.format(countPercentage(wordList.size())) + " %");
	}
}
